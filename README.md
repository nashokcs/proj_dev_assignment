**How to clone and build this repo:**

1. Open Command Prompt and type **git clone https://nashokcs@bitbucket.org/nashokcs/proj_dev_assignment.git** and press Enter.
2. Open the cloned repo folder ** proj_dev_assignment** from command Prompt.
3. Type **npm install** command.
4. Type **npx lite-server** command to run the node server.
5. Open the browser and type [https://localhost:3000](https://localhost:3000).

---

## Create a html file

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **sample.html**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

---

LICENSE
Copyright 2021 Ashokchakravarthi Nagarajan.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.